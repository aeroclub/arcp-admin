import React from 'react';
import { Admin, Resource, ListGuesser } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';
import { UserList } from './users';
import { PostList } from './post';

const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');

const App = () => (
<Admin dataProvider={dataProvider} >
  <Resource name="users" list={UserList} />
  <Resource name="posts" list={ListGuesser} />
  <Resource name="posts" list={PostList} />
</Admin>
);

export default App;
